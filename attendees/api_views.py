from django.http import JsonResponse

from .models import Attendee
from events.models import Conference
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_id)
        return JsonResponse(
            {"attendees": attendees}, encoder=AttendeeListEncoder, safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference ID"},
                status=400,
            )
        attendee = Attendee.objects.create(**content)
        return JsonResponse(attendee, encoder=AttendeeListEncoder, safe=False)


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


class ShowAttendeeEncoder(ModelEncoder):
    model = Attendee
    properties = ["email", "name", "company_name", "created", "conference"]

    encoders = {"conference": ConferenceListEncoder()}


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, id):
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(attendee, encoder=ShowAttendeeEncoder, safe=False)
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        attendee = Attendee.objects.filter(id=id).update(**content)
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(attendee, encoder=ShowAttendeeEncoder, safe=False)

    # """
    # api_list_attendees
    # Lists the attendees names and the link to the attendee
    # for the specified conference id.

    # Returns a dictionary with a single key "attendees" which
    # is a list of attendee names and URLS. Each entry in the list
    # is a dictionary that contains the name of the attendee and
    # the link to the attendee's information.

    # {
    #     "attendees": [
    #         {
    #             "name": attendee's name,
    #             "href": URL to the attendee,
    #         },
    #         ...
    #     ]
    # }
    # """
    # {
    #     "email": attendee.email,
    #     "name": attendee.name,
    #     "company_name": attendee.company_name,
    #     "created": attendee.created,
    #     "conference": {
    #         "name": attendee.conference.name,
    #         "href": attendee.conference.get_api_url(),
    #     },
    # }

    # response = []
    # conference = Conference.objects.get(id=conference_id)
    # attendees = Attendee.objects.filter(conference=conference)
    # for a in attendees:
    #     response.append(
    #         {
    #             "name": attendees.name,
    #             "href": attendees.get_api_url(),
    #         }
    #     )
    # return JsonResponse({"attendees": response})
