from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_pexels(city, state):
    url = f"https://api.pexels.com/v1/search?query={city}%20{state}"
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(
        url,
        headers=headers,
    )
    pic = json.loads(response.content)

    picture = {"picture_url": pic["photos"][0]["url"]}

    return picture


def get_geocode(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},840&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    geoinfo = json.loads(response.content)
    print(geoinfo)
    geocode = {"lat": geoinfo[0]["lat"], "lon": geoinfo[0]["lon"]}
    return geocode


def get_weather(lat, lon):
    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    weatherinfo = json.loads(response.content)
    weather = {
        "temperature": weatherinfo["main"]["temp"],
        "description": weatherinfo["weather"][0]["description"],
    }
    return weather
